import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "psu trang",
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.account_circle_rounded),
          title: Text('My app'),
          actions: [
            IconButton(
                onPressed: (){},
                icon: Icon(Icons.add_ic_call_rounded),
            ),
            IconButton(
              onPressed: (){},
              icon: Icon(Icons.ac_unit_sharp),
            ),
          ],
        ),
        body: Column(
          children:[
            Image.asset('asset/mon.jpg'),
            Text('นางสาว ธมนวรรณ อินทวงศ์  ', style: TextStyle(height: 2, fontSize: 20),),
            Text('6350110008', style: TextStyle(height: 2, fontSize: 20),),
          ],
        ),
      ),
    );
  }
}
